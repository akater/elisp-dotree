;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'dotree
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  source-files-in-order '("dotree")
  site-lisp-config-prefix "30"
  license "GPL-3")

(advice-add 'fakemake-prepare :before
            (lambda ()
              (warn "This package is obsolete.  Use shmu instead: https://framagit.org/akater/elisp-shmu")))
